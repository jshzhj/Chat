package router

import (
	"net/http"
	"../ctrl"
	"time"
	"log"
)

//中间件:打印每个http请求的所花费的时间
func timeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(wr http.ResponseWriter, r *http.Request) {
		timeStart := time.Now()

		// next handler
		next.ServeHTTP(wr, r)

		timeElapsed := time.Since(timeStart)
		log.Println(timeElapsed)
	})
}

func Router(mux *http.ServeMux) {
	mux.Handle("/user/login", timeMiddleware(http.HandlerFunc(ctrl.UserLogin)))
	mux.Handle("/user/register", timeMiddleware(http.HandlerFunc(ctrl.UserRegister)))
	mux.Handle("/contact/loadcommunity", timeMiddleware(http.HandlerFunc(ctrl.LoadCommunity)))
	mux.Handle("/contact/loadfriend", timeMiddleware(http.HandlerFunc(ctrl.LoadFriend)))
	mux.Handle("/contact/joincommunity", timeMiddleware(http.HandlerFunc(ctrl.JoinCommunity)))
	mux.Handle("/contact/createcommunity", timeMiddleware(http.HandlerFunc(ctrl.CreateCommunity)))
	mux.Handle("/contact/addfriend", timeMiddleware(http.HandlerFunc(ctrl.Addfriend)))
	mux.Handle("/chat", timeMiddleware(http.HandlerFunc(ctrl.Chat)))
	mux.Handle("/attach/upload", timeMiddleware(http.HandlerFunc(ctrl.Upload)))
}
