# Chat

#### 介绍
使用golang构建的聊天系统,支持加好友,建群,加群,单聊,群聊,发送文字,表情,图片,视频,语音(原理和图片一样,请自行扩展),支持分布式部署等

#### 软件架构
软件架构说明

#### 目录结构

1. args: 结构体
2. asset: 静态资源,css,js文件等
3. conf: 配置文件目录
4. core: 核心目录,存放一些全局的东西,目前没啥东西在里面
5. ctrl: 相当于mvc的 c,控制器目录
6. mnt: 资源文件上传保存目录
7. model：相当于mvc的 m,模型目录
8. release: 执行build.sh脚本之后,项目打包目录
9. router: 路由
10. service: 控制器方法封装目录
11. test: 测试目录,目前还不完善,写的也比较随意
12. util: 公共函数目录
13. view: html文件存放目录

#### 安装
1. 源码:git clone https://gitee.com/zhj-open-source/Chat.git

#### 使用教程
1. 修改conf/conf.ini中mysql的配置参数(改成你的mysql参数),直接启动程序即可,会自动创建mysql表结构,也可以运行mysql表结构文件chat.sql,创建表结构
2. 进入release目录,进入对应的操作系统,直接运行chat程序即可，启动服务,访问本地8080端口,eg: http://127.0.0.1:8080/user/login.shtml
3. 注册,登陆,just do it!!!
4. 如果你想打包编译,则直接执行build.sh这个脚本,打包目录在release目录中

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)