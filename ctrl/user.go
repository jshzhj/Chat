package ctrl

import (
	"net/http"
	"../service"
	"../util"
	"../model"
)

//定义一个UserService的实例
var userService service.UserService

func UserLogin(writer http.ResponseWriter, request *http.Request) {
	//如何获得参数？
	//解析参数
	request.ParseForm()

	mobile := request.PostForm.Get("mobile")
	passwd := request.PostForm.Get("passwd")

	user, err := userService.Login(mobile, passwd)

	if (err != nil) {
		util.RespFail(writer, err.Error())
	} else {
		util.RespOk(writer, user, "")
	}
}

func UserRegister(writer http.ResponseWriter, request *http.Request) {
	//如何获得参数？

	//解析参数
	request.ParseForm()

	mobile := request.PostForm.Get("mobile")
	plainpwd := request.PostForm.Get("passwd")
	//nickname := fmt.Sprintf("user%06d", rand.Int31n(100))
	nickname := request.PostForm.Get("nickname")
	avatar := request.PostForm.Get("avatar")
	memo := request.PostForm.Get("memo")
	sex := model.SEX_UNKNOW

	user, err := userService.Register(mobile, plainpwd, nickname, avatar, sex,memo)

	if (err != nil) {
		util.RespFail(writer, err.Error())
	} else {
		util.RespOk(writer, user, "")
	}
}
