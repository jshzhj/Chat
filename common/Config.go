package common

import (
	"github.com/Unknwon/goconfig"
	"github.com/smokezl/govalidators"
)

var (
	G_config *Config
)
//解析配置文件到此结构体
type Config struct {
	HttpServerPort string `validate:"required"`
	HttpServerHost string `validate:"required"`

	MysqlIp       string `validate:"required"`
	MysqlPort     string `validate:"required"`
	MysqlDatabase string `validate:"required"`
	MysqlCharset  string `validate:"required"`
	MysqlUserName string `validate:"required"`
	MysqlPassword string `validate:"required"`
	MysqlShowSql  string   `validate:"required"`
	MysqlConn     string `validate:"required"`

	RedisAddress string `validate:"required"`

	DistributedType string `validate:"required"`
	//DistributedType=2时,填写如下参数
	Distributed_net_IPv4_a string `validate:"required"`
	Distributed_net_IPv4_b string `validate:"required"`
	Distributed_net_IPv4_c string `validate:"required"`
	Distributed_netmask    string `validate:"required"`
	Distributed_port       string `validate:"required"`
	//DistributedType=3时,填写如下参数
	Distributed_rabbitmq_user     string `validate:"required"`
	Distributed_rabbitmq_password string `validate:"required"`
	Distributed_rabbitmq_ip       string `validate:"required"`
	Distributed_rabbitmq_port     string `validate:"required"`
}

//解析配置文件到全局配置结构体中
func InitConfig() (err error) {
	var (
		conf    *Config
		go_conf *goconfig.ConfigFile
	)

	conf = &Config{}

	if go_conf, err = goconfig.LoadConfigFile("./conf/conf.ini"); err != nil {
		return
	}

	//端口
	if conf.HttpServerPort, err = go_conf.GetValue("server", "port"); err != nil {
		return
	}

	//域名
	if conf.HttpServerHost, err = go_conf.GetValue("server", "host"); err != nil {
		return
	}

	//mysql部分
	if conf.MysqlIp, err = go_conf.GetValue("mysql", "ip"); err != nil {
		return
	}

	if conf.MysqlPort, err = go_conf.GetValue("mysql", "port"); err != nil {
		return
	}

	if conf.MysqlDatabase, err = go_conf.GetValue("mysql", "database"); err != nil {
		return
	}

	if conf.MysqlCharset, err = go_conf.GetValue("mysql", "charset"); err != nil {
		return
	}

	if conf.MysqlUserName, err = go_conf.GetValue("mysql", "username"); err != nil {
		return
	}

	if conf.MysqlPassword, err = go_conf.GetValue("mysql", "password"); err != nil {
		return
	}

	if conf.MysqlShowSql, err = go_conf.GetValue("mysql", "show_sql"); err != nil {
		return
	}

	if conf.MysqlConn, err = go_conf.GetValue("mysql", "max_conn"); err != nil {
		return
	}

	//redis部分
	if conf.RedisAddress, err = go_conf.GetValue("redis", "address"); err != nil {
		return
	}

	//DistributedType分布式消息传送类型选择
	if conf.DistributedType, err = go_conf.GetValue("distributed", "type"); err != nil {
		return
	}

	//UDP传递消息配置
	if conf.Distributed_net_IPv4_a, err = go_conf.GetValue("distributed", "net_IPv4_a"); err != nil {
		return
	}
	if conf.Distributed_net_IPv4_b, err = go_conf.GetValue("distributed", "net_IPv4_b"); err != nil {
		return
	}
	if conf.Distributed_net_IPv4_c, err = go_conf.GetValue("distributed", "net_IPv4_c"); err != nil {
		return
	}
	if conf.Distributed_netmask, err = go_conf.GetValue("distributed", "netmask"); err != nil {
		return
	}
	if conf.Distributed_port, err = go_conf.GetValue("distributed", "port"); err != nil {
		return
	}

	//RabbitMQ传递消息配置
	if conf.Distributed_rabbitmq_user, err = go_conf.GetValue("distributed", "rabbitmq_user"); err != nil {
		return
	}
	if conf.Distributed_rabbitmq_password, err = go_conf.GetValue("distributed", "rabbitmq_password"); err != nil {
		return
	}
	if conf.Distributed_rabbitmq_ip, err = go_conf.GetValue("distributed", "rabbitmq_ip"); err != nil {
		return
	}
	if conf.Distributed_rabbitmq_port, err = go_conf.GetValue("distributed", "rabbitmq_port"); err != nil {
		return
	}
	//配置文件参数验证(主要验证是否为空)
	validator := govalidators.New()
	validator.SetValidators(map[string]interface{}{
		"required": &govalidators.RequiredValidator{
			EMsg: "项目配置文件:cong.ini~[name]参数必填!",
		},
	})
	errList := validator.Validate(conf)
	if errList != nil {
		for _, err := range errList {
			G_logger.Fatal(err.Error())
		}
	}
	G_config = conf
	return
}
