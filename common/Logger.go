package common

import (
	"github.com/sirupsen/logrus"
	"os"
)

var (
	G_logger *logrus.Logger
)

func InitLogger() {
	G_logger = logrus.New()

	// 为当前logrus实例设置消息的输出，同样地，
	// 可以设置logrus实例的输出到任意io.writer
	G_logger.Out = os.Stdout

	// 为当前logrus实例设置消息输出格式为json格式。
	// 同样地，也可以单独为某个logrus实例设置日志级别和hook，这里不详细叙述。
	G_logger.Formatter = &logrus.JSONFormatter{}
	G_logger.Info("日志模块初始化成功")
}
