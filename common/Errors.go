package common

import (
	"errors"
)

var (
	ERR_HTTP_SERVER_PORT_NOT_SET = errors.New("HTTP服务端口没有设置")
	ERR_HTTP_SERVER_HOST_NOT_SET = errors.New("HTTP域名没有设置")
	ERR_MYSQL_IP_NOT_SET         = errors.New("mysql的ip地址没有设置")
)
