/*
 Navicat Premium Data Transfer

 Source Server         : 张慧君Golang-Chat数据库
 Source Server Type    : MySQL
 Source Server Version : 50644
 Source Host           : 106.12.21.94:3306
 Source Schema         : chat

 Target Server Type    : MySQL
 Target Server Version : 50644
 File Encoding         : 65001

 Date: 16/01/2020 19:41:44
*/


drop database if exists `chat`;
create database `chat` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
use chat;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `community`;
CREATE TABLE `community` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '群名称',
  `ownerid` bigint(20) DEFAULT NULL COMMENT '群主ID',
  `icon` varchar(250) DEFAULT NULL COMMENT '群logo',
  `cate` int(11) DEFAULT NULL,
  `memo` varchar(120) DEFAULT NULL COMMENT '群描述',
  `createat` datetime DEFAULT NULL COMMENT '群创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群表';

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ownerid` bigint(20) DEFAULT NULL COMMENT '添加人id',
  `dstobj` bigint(20) DEFAULT NULL COMMENT '被添加人id',
  `cate` int(11) DEFAULT NULL COMMENT '类型',
  `memo` varchar(120) DEFAULT NULL COMMENT '描述',
  `createat` datetime DEFAULT NULL COMMENT '添加创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='好友关系表';

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
  `passwd` varchar(40) DEFAULT NULL COMMENT '密码',
  `avatar` varchar(150) DEFAULT NULL COMMENT '头像',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `salt` varchar(10) DEFAULT NULL COMMENT '密码盐',
  `online` int(10) DEFAULT NULL COMMENT '是否在线',
  `token` varchar(40) DEFAULT NULL COMMENT 'token',
  `memo` varchar(140) DEFAULT NULL,
  `createat` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户表';

SET FOREIGN_KEY_CHECKS = 1;
