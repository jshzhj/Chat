#!/bin/sh
rm -rf ./release

mkdir -p release/windows release/mac release/linux

#go build -o chat
#编译linux二进制程序
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o chat
chmod -R 777 ./chat
mv chat ./release/linux
#编译mac二进制程序
go build -o chat
chmod -R 777 ./chat
mv chat ./release/mac
#编译windows二进制程序
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -o chat
chmod -R 777 ./chat
mv chat ./release/windows

cp -r ./asset ./release/mac
cp -r ./view ./release/mac
cp -r ./conf ./release/mac

cp -r ./asset ./release/linux
cp -r ./view ./release/linux
cp -r ./conf ./release/linux

cp -r ./asset ./release/windows
cp -r ./view ./release/windows
cp -r ./conf ./release/windows

#linux下运行
# nohup ./chat >> ./log.log 2>&1 &


