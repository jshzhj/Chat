package main
//柴树斌的go-web书籍,很棒 https://books.studygolang.com/advanced-go-programming-book/ch5-web/ch5-03-middleware.html
import (
	"net/http"
	"log"
	"os"
	"time"
)

var logger = log.New(os.Stdout, "", 0)

func hello(wr http.ResponseWriter, r *http.Request) {
	wr.Write([]byte("hello"))
}

func timeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(wr http.ResponseWriter, r *http.Request) {
		timeStart := time.Now()

		// next handler
		next.ServeHTTP(wr, r)

		timeElapsed := time.Since(timeStart)
		logger.Println(timeElapsed)
	})
}

func main() {

	http.Handle("/", timeMiddleware(http.HandlerFunc(hello)))

	err := http.ListenAndServe(":8080", nil)

	if err !=nil{
		log.Fatal(err)
	}

}
