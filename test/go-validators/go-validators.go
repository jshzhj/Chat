package main

import (
	"github.com/smokezl/govalidators"
	"fmt"
)

type Student struct {
	Uid          int64    `validate:"required||integer=10000,_"`
	Name         string   `validate:"required||string=1,5"`
	Age          int64    `validate:"required||integer=10,30"`
	Sex          string   `validate:"required||in=male,female"`
	Email        string   `validate:"email||user||vm"`
	PersonalPage string   `validate:"url"`
	Hobby        []string `validate:"array=_,2||unique||in=swimming,running,drawing"`
	CreateTime   string   `validate:"datetime"`
}

func main() {
	validator := govalidators.New()
	student := &Student{
		Uid:          1234567,
		Name:         "张三1111",
		Age:          31,
		Sex:          "male1",
		Email:        "@qq.com",
		PersonalPage: "www.abcd.com",
		Hobby:        []string{"swimming", "singing"},
		CreateTime:   "2018-03-03 05:60:00",
	}
	//自定义错误信息
	validator.SetValidators(map[string]interface{}{
		"string": &govalidators.StringValidator{
			Range: govalidators.Range{
				RangeEMsg: map[string]string{
					"between": "[name] 长度必须在 [min] 和 [max] 之间",
				},
			},
		},
		"integer": &govalidators.IntegerValidator{
			Range: govalidators.Range{
				RangeEMsg: map[string]string{
					"between": "[name] 的值必须在 [min] 和 [max] 之间",
				},
			},
		},
		"in": &govalidators.InValidator{
			EMsg: "[name] 的值必须为 [args] 中的一个",
		},
		"email": &govalidators.EmailValidator{
			EMsg: "[name] 不是一个有效的email地址",
		},
		"url": &govalidators.UrlValidator{
			EMsg: "[name] 不是一个有效的url地址",
		},
		"datetime": &govalidators.DateTimeValidator{
			EMsg: "[name] 不是一个有效的日期",
		},
		"unique": &govalidators.UniqueValidator{
			EMsg: "[name] 不是唯一的",
		},
	})
	errList := validator.Validate(student)
	if errList != nil {
		for _, err := range errList {
			fmt.Println("err:", err)
		}
	}
}
