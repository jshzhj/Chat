package main

import (
	"github.com/muesli/cache2go"
	"fmt"
	"time"
)

// Keys & values in cache2go can be of arbitrary types, e.g. a struct.
type myStruct struct {
	text     string
	moreData []byte
}

func main() {
	//首次访问一个新的缓存表将创建它
	cache := cache2go.Cache("myCache")

	//我们将会放一个新的元素在缓存.没有被访问的时间超过5s，它将会过期
	val := myStruct{"This is a test!", []byte{}}
	cache.Add("someKey", 5*time.Second, &val)

	//让我们从缓存中检索这个元素
	res, err := cache.Value("someKey")
	if err == nil {
		fmt.Println("Found value in cache:", res.Data().(*myStruct).text)
	} else {
		fmt.Println("Error retrieving value from cache:", err)
	}

	//等待这个元素过期
	time.Sleep(6 * time.Second)
	res, err = cache.Value("someKey")
	if err != nil {
		fmt.Println("Item is not cached (anymore).")
	}

	//添加另一个不会过期的元素
	cache.Add("someKey", 0, &val)

	//cache2go支持一些方便的回调和加载机制
	cache.SetAboutToDeleteItemCallback(func(e *cache2go.CacheItem) {
		fmt.Println("Deleting:", e.Key(), e.Data().(*myStruct).text, e.CreatedOn())
	})

	//从缓存中删除
	cache.Delete("someKey")

	//并擦除整个缓存表
	cache.Flush()
}
