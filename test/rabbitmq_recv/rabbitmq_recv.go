package main

import (
	"log"
	"github.com/streadway/amqp"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@106.12.21.94:5672/")
	if err !=nil{
		log.Fatalln(err.Error())
	}

	defer conn.Close()

	ch, err := conn.Channel()
	if err !=nil{
		log.Fatalln(err.Error())
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"zhj-exchange",   // name
		"fanout", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	if err !=nil{
		log.Fatalln(err.Error())
	}

	q, err := ch.QueueDeclare(
		"zhj-queue-2",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err !=nil{
		log.Fatalln(err.Error())
	}

	err = ch.QueueBind(
		q.Name, // queue name
		"",     // routing key
		"zhj-exchange", // exchange
		false,
		nil)
	if err !=nil{
		log.Fatalln(err.Error())
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err !=nil{
		log.Fatalln(err.Error())
	}

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf(" [x] %s", d.Body)
		}
	}()

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}