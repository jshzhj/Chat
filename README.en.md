# Chat

#### Description
使用golang构建的聊天系统,支持加好友,建群,加群,单聊,群聊,发送文字,表情,图片,视频,语音等

#### Software Architecture
Software architecture description

#### 目录说明

1. args:
2. asset: 静态资源目录,css,js,表情包等
3. conf: 项目配置文件目录
4. core: 项目核心目录
5. ctrl: 控制器目录
6. mnt: 上传图片,视频，语音等文件存放目录
7. model: 模型目录
8. release: 项目编译发布目录
9. service: service层
10. test: 测试目录
11. util: 公共函数库
12. view: html文件存放位置
13 build.sh: 项目编译打包发布脚本
14 main.go: 项目主入口文件

#### Installation

1. xxxx
2. xxxx
3. xxxx



#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)