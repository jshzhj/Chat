package service

import (
	"../model"
	"github.com/pkg/errors"
	"fmt"
	"math/rand"
	"../util"
	"time"
	"../common"
)

//相当于php的类
type UserService struct {
}

//注册函数
func (s *UserService) Register(mobile, plainpwd, nickname, avatar, sex ,memo string) (user model.User, err error) {
	//检测手机号是否存在
	tmp := model.User{}

	_, err = common.DbEngin.Where("mobile=?", mobile).Get(&tmp)
	if err != nil {
		return tmp, err
	}
	//如果存在,则已经存在
	if tmp.Id > 0 {
		return tmp, errors.New("该手机号已经注册")
	}
	//否则,插入数据库
	tmp.Mobile = mobile
	tmp.Avatar = avatar
	tmp.Nickname = nickname
	tmp.Sex = sex
	tmp.Memo = memo

	//生成随机数
	tmp.Salt = fmt.Sprintf("%06d", rand.Int31n(10000))
	tmp.Passwd = util.MakePasswd(plainpwd, tmp.Salt)
	tmp.Createat = time.Now()
	tmp.Token = fmt.Sprintf("%08d", rand.Int31())
	//MD5加密

	//插入数据库
	_, err = common.DbEngin.InsertOne(&tmp)
	//前端恶意插入特殊字符,数据库连接操作失败

	//返回用户信息
	return user, err
}

//登陆函数
func (s *UserService) Login(mobile, plainpwd string) (user model.User, err error) {
	//通过手机号查询用户
	var tmp model.User

	common.DbEngin.Where("mobile = ?", mobile).Get(&tmp)
	//查询到了比对密码
	if (tmp.Id == 0) {
		//用户没查到
		return tmp, errors.New("该用户不存在!")
	}
	//比对密码是否正确
	if !util.ValidatePasswd(plainpwd, tmp.Salt, tmp.Passwd) {
		return tmp, errors.New("密码不正确")
	}
	//刷新token,返回数据：安全起见,登陆一次,刷新一次
	str := fmt.Sprintf("%d", time.Now().Unix())
	token := util.MD5Encode(str)
	tmp.Token = token
	common.DbEngin.Id(tmp.Id).Cols("token").Update(&tmp)
	return tmp, nil
}

//查找某个用户
func (s *UserService) Find(userId int64) (user model.User) {
	//通过用户id查询用户
	var tmp model.User
	common.DbEngin.ID(userId).Get(&tmp)
	//返回用户信息
	return tmp
}
